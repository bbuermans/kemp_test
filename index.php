<?php
require 'vendor/autoload.php';

use BTCMS\BTCMS_Grabber;

try {
    $conn = new PDO('mysql:host=127.0.0.1;dbname=test', 'root', '');
    $BTCMS = new BTCMS_Grabber($conn);

    # Testing output
    // $output['glossary'] = $BTCMS->getAllGlossaryItems();
    $output['news'] = $BTCMS->getPagesByParentId(83);
    $output['professionals'] = $BTCMS->getProfessionalsByParentId(86);
    dd($output);

    # Export rules
    // $BTCMS->generateGlossaryJSONExport();
    // $BTCMS->generateProffessionalsJSONExport();
    // $BTCMS->generateNewsJSONExport();

    # unset connection
    $conn = null;

} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}

