<?php namespace BTCMS;

use Cocur\Slugify\Slugify;
use PDO;

class BTCMS_Grabber {

    private $conn;
    private $slugify;


    public function __construct($conn)
    {
        $this->conn = $conn;
        $this->slugify = new Slugify();
    }


    public function getProfessionalsByParentId($parentId)
    {
        $statement = $this->conn->prepare(
            'SELECT *
            FROM pages
            WHERE parent_id = :parentId
            ORDER BY seq;'
        );

        $statement->execute([
            ':parentId' => $parentId
        ]);

        return $this->getAllProfessionalsData($statement->fetchAll(PDO::FETCH_ASSOC));
    }


    private function getAllProfessionalsData(array $results)
    {
        $output = [];

        foreach ($results as $result) {
            $head = [
                'id'         => $result['groep_id'],
                'achternaam' => $result['groep_naam'],
                'slug'       => $result['seolabel'],
                'title'      => ucwords(str_replace('-', ' ', $result['seolabel'])),
                'is_active'  => $result['active'] === 'Y',
            ];
            $data = $this->getProfessionalFieldData($result['groep_id']);
            $fields = $this->mapProfessionalData($data);
            // $fields = $data;

            $output[] = array_merge($head, $fields);
        }

        return $output;
    }

    private function getProfessionalFieldData($id)
    {
        $statement = $this->conn->prepare(
            'SELECT
                pagekenmerken.kenmerknaam as name,
                pagewaardes.kenmerkwaarde as value,
                pagekenmerken.kenmerktype as type
            FROM pagewaardes
            INNER JOIN pagekenmerken
                ON pagekenmerken.id = pagewaardes.kenmerkid
            WHERE pagewaardes.itemid = :itemId;'
        );

        $statement->execute([
            ':itemId' => $id
        ]);

        return $this->mapFields($statement->fetchAll(PDO::FETCH_ASSOC));
    }



    public function getPagesByParentId($parentId)
    {
        $statement = $this->conn->prepare(
            'SELECT *
            FROM pages
            WHERE parent_id = :parentId
            ORDER BY seq;'
        );

        $statement->execute([
            ':parentId' => $parentId
        ]);

        return $this->getAllPageData($statement->fetchAll(PDO::FETCH_ASSOC));
    }


    private function getAllPageData(array $results)
    {
        $output = [];

        foreach ($results as $result) {
            $output[] = [
                'id'         => $result['groep_id'],
                'title'      => $result['groep_naam'],
                'slug'       => $result['seolabel'],
                'is_active'  => $result['active'] === 'Y',
                'newsLayout' => $this->getInnerBlocks($result['groep_id'])
            ];
        }

        return $output;
    }


    public function getInnerBlocks($id)
    {
        $statement = $this->conn->prepare(
            'SELECT *
            FROM koppel_pageitem_aan_page
            WHERE groepid = :groepId
            ORDER BY seq;'
        );

        $statement->execute([
            ':groepId' => $id
        ]);

        return $this->getAllBlockData($statement->fetchAll(PDO::FETCH_ASSOC));
    }


    private function getAllBlockData($blocks)
    {
        $output = [];

        foreach ($blocks as $block) {
            $blockHead = $this->getBlockHead($block['itemid']);
            $blockHead = reset($blockHead);

            $head = [
                'heading'  => $blockHead['titel'],
                'type'   => $blockHead['typenaam'],
                'isActive' => $blockHead['active'] === 'Y'
            ];
            $content = $this->getBlockContents($block['itemid']);
            $content = $this->mapBlocksData($content, $blockHead['typenaam']);

            $output[] = array_merge($head, $content);
        }

        return $output;
    }


    public function getBlockHead($id)
    {
        $statement = $this->conn->prepare(
            'SELECT
                pageitems.titel,
                pageitems.active,
                pageitemtype.typenaam
            FROM pageitems
            LEFT JOIN pageitemtype
                ON pageitemtype.id = pageitems.itemtypeid
            WHERE pageitems.id = :itemId;'
        );

        $statement->execute([
            ':itemId' => $id
        ]);

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }


    public function getBlockContents($id)
    {
        $statement = $this->conn->prepare(
            'SELECT
                pageitemkenmerken.kenmerknaam as name,
                pageitemwaardes.kenmerkwaarde as value
            FROM pageitemwaardes
            LEFT JOIN pageitemkenmerken
                ON pageitemwaardes.kenmerkid = pageitemkenmerken.id
            WHERE itemid = :itemId;'
        );

        $statement->execute([
            ':itemId' => $id
        ]);

        return $this->mapFields($statement->fetchAll(PDO::FETCH_ASSOC));
    }


    public function getAllGlossaryItems()
    {
        $statement = $this->conn->prepare(
            'SELECT
                pageitemszoek.id AS id,
                pageitemszoek.titel AS title,
                pageitemszoek.active AS active,
                pageitemszoek.seolabel AS slug,
                nl.kenmerkwaarde AS content_nl,
                en.kenmerkwaarde AS content_en,
                split.kenmerkwaarde AS splitstr
            FROM koppel_pageitemzoek_aan_pagezoek
                INNER JOIN pageitemszoek
                    ON koppel_pageitemzoek_aan_pagezoek.itemid = pageitemszoek.id
                INNER JOIN pageitemwaardeszoek nl
                    ON koppel_pageitemzoek_aan_pagezoek.itemid = nl.itemid
                INNER JOIN pageitemwaardeszoek en
                    ON koppel_pageitemzoek_aan_pagezoek.itemid = en.itemid
                INNER JOIN pageitemwaardeszoek split
                    ON koppel_pageitemzoek_aan_pagezoek.itemid = split.itemid
            WHERE nl.kenmerkid = 122 AND en.kenmerkid = 123 AND split.kenmerkid = 124'
        );

        $statement->execute();

        return $this->mapGlossaryData($statement->fetchAll(PDO::FETCH_ASSOC));
    }


    private function mapGlossaryData($results)
    {
        $output = [
            'nl' => [],
            'en' => []
        ];

        foreach ($results as $r) {

            // Filter out pesky Documentation item which breaks loop
            // This page will be setup as a seperate CraftCMS single
            if ($r['id'] !== '428') {

                $s = explode('[SPLITTER]', $r['splitstr']);

                $title_nl = $s[0] ?: '';
                $title_en = $s[1] ?: '';

                // Create slugify instance:
                // Needed to create slugs for EN glossary items
                $slug_en = $this->slugify->slugify($title_en);

                // Synonyms
                $syns_nl = $s[2] ?: [];
                if ($syns_nl) {
                    $syns_nl = explode(',', $syns_nl);
                    $syns_nl = array_map('trim', $syns_nl);
                    $syns_nl = array_map('ucfirst', $syns_nl);
                }

                $syns_en = $s[3] ?: [];
                if ($syns_en) {
                    $syns_en = explode(',', $syns_en);
                    $syns_en = array_map('trim', $syns_en);
                    $syns_en = array_map('ucfirst', $syns_en);
                }

                // BTCMS Flag Settings
                $vlag_vla = $s[4];
                $vlag_ace = $s[5];
                $vlag_cew = $s[6];
                $vlag_csg = $s[7];
                $vlag_cnl = $s[8];
                $vlag_bs = $s[9];

                $vlag_aandoening = $s[10];
                $vlag_onderzoek = $s[11];
                $vlag_behandeling = $s[12];
                $vlag_praktisch = $s[13];
                $vlag_diagnostisch = $s[14];
                $vlag_begeleiding = $s[15];

                $vlag_extra_1 = $s[16];
                $vlag_extra_2 = $s[17];
                $vlag_extra_3 = $s[18];
                $vlag_extra_4 = $s[19];
                $vlag_extra_5 = $s[20];
                $vlag_extra_6 = $s[21];

                $vlag_patient = $s[22];
                $vlag_prof = $s[23];

                $vlag_sub_ace = $s[24];
                $vlag_sub_csg = $s[25];
                $vlag_sub_cew = $s[26];
                $vlag_sub_mijn = (isset($s[27])) ? $s[27] : '';

                // Map and trim to new array
                $f = [
                    'vlag_vla'          => trim(strtolower($vlag_vla)),
                    'vlag_ace'          => trim(strtolower($vlag_ace)),
                    'vlag_cew'          => trim(strtolower($vlag_cew)),
                    'vlag_csg'          => trim(strtolower($vlag_csg)),
                    'vlag_cnl'          => trim(strtolower($vlag_cnl)),
                    'vlag_bs'           => trim(strtolower($vlag_bs)),
                    'vlag_aandoening'   => trim(strtolower($vlag_aandoening)),
                    'vlag_onderzoek'    => trim(strtolower($vlag_onderzoek)),
                    'vlag_behandeling'  => trim(strtolower($vlag_behandeling)),
                    'vlag_praktisch'    => trim(strtolower($vlag_praktisch)),
                    'vlag_diagnostisch' => trim(strtolower($vlag_diagnostisch)),
                    'vlag_begeleiding'  => trim(strtolower($vlag_begeleiding)),
                    'vlag_extra_1'      => trim(strtolower($vlag_extra_1)),
                    'vlag_extra_2'      => trim(strtolower($vlag_extra_2)),
                    'vlag_extra_3'      => trim(strtolower($vlag_extra_3)),
                    'vlag_extra_4'      => trim(strtolower($vlag_extra_4)),
                    'vlag_extra_5'      => trim(strtolower($vlag_extra_5)),
                    'vlag_extra_6'      => trim(strtolower($vlag_extra_6)),
                    'vlag_patient'      => trim(strtolower($vlag_patient)),
                    'vlag_prof'         => trim(strtolower($vlag_prof)),
                    'vlag_sub_ace'      => trim(strtolower($vlag_sub_ace)),
                    'vlag_sub_csg'      => trim(strtolower($vlag_sub_csg)),
                    'vlag_sub_cew'      => trim(strtolower($vlag_sub_cew)),
                    'vlag_sub_mijn'     => trim(strtolower($vlag_sub_mijn)),
                ];

                // Mapping flags to CraftCMS category ID's
                $categories = [];

                if ($f['vlag_ace'] === 'ja') {
                    $categories[] = 913;

                    if ($f['vlag_sub_ace'] === 'kind epilepsie') {
                        $categories[] = 923;
                    } elseif ($f['vlag_sub_ace'] === 'kind aanvallen') {
                        $categories[] = 924;
                    } elseif ($f['vlag_sub_ace'] === 'u epilepsie') {
                        $categories[] = 925;
                    } elseif ($f['vlag_sub_ace'] === 'u aanvallen') {
                        $categories[] = 926;
                    }

                    if ($f['vlag_onderzoek'] === 'ja') {
                        $categories[] = 927;
                    }

                    if ($f['vlag_behandeling'] === 'ja') {
                        $categories[] = 928;
                    }

                    if ($f['vlag_begeleiding'] === 'ja') {
                        $categories[] = 929;
                    }

                    if ($f['vlag_praktisch'] === 'ja') {
                        $categories[] = 938;
                    }
                }

                if ($f['vlag_csg'] === 'ja') {
                    $categories[] = 914;

                    if ($f['vlag_diagnostisch'] === 'ja') {
                        $categories[] = 930;
                    }

                    if ($f['vlag_onderzoek'] === 'ja') {
                        $categories[] = 931;
                    }

                    if ($f['vlag_behandeling'] === 'ja') {
                        $categories[] = 932;
                    }

                    if ($f['vlag_praktisch'] === 'ja') {
                        $categories[] = 939;
                    }
                }

                if ($f['vlag_cnl'] === 'ja') {
                    $categories[] = 915;

                    if ($f['vlag_diagnostisch'] === 'ja') {
                        $categories[] = 933;
                    }

                    if ($f['vlag_onderzoek'] === 'ja') {
                        $categories[] = 934;
                    }

                    if ($f['vlag_praktisch'] === 'ja') {
                        $categories[] = 940;
                    }
                }

                if ($f['vlag_cew'] === 'ja') {
                    $categories[] = 916;

                    // Woonvormen
                    if ($f['vlag_sub_cew'] === 'wonen') {
                        $categories[] = 935;
                    } elseif ($f['vlag_sub_cew'] === 'dagbesteding') {
                        $categories[] = 936;
                    }  elseif ($f['vlag_sub_cew'] === 'vrije tijd') {
                        $categories[] = 937;
                    }

                    if ($f['vlag_praktisch'] === 'ja') {
                        $categories[] = 940;
                    }
                }

                if ($f['vlag_bs'] === 'ja') {
                    $categories[] = 917;
                }

                if (strlen($f['vlag_sub_mijn']) > 0 && $f['vlag_sub_mijn'] !== 'nee') {
                    $categories[] = 918;

                    if ($f['vlag_sub_mijn'] === 'inhoud') {
                        $categories[] = 942;
                    } elseif ($f['vlag_sub_mijn'] === 'toegang') {
                        $categories[] = 943;
                    } elseif ($f['vlag_sub_mijn'] === 'e-consult') {
                        $categories[] = 944;
                    } elseif ($f['vlag_sub_mijn'] === 'werking') {
                        $categories[] = 945;
                    }
                }

                $output['nl'][] = [
                    'id'         => trim($r['id']),
                    'active'     => $r['active'] === 'Y',
                    'title_nl'   => trim(ucfirst($title_nl)),
                    'slug_nl'    => trim($r['slug']),
                    'syn_nl'     => trim(implode(',', $syns_nl)),
                    'content_nl' => trim($r['content_nl']),
                    'categories' => implode('|', $categories),
                ];

                if (trim(ucfirst($title_en))) {
                    $output['en'][] = [
                        'id'         => trim($r['id']),
                        'active'     => $r['active'] === 'Y',
                        'title_en'   => trim(ucfirst($title_en)),
                        'slug_en'    => trim($slug_en),
                        'syn_en'     => trim(implode(',', $syns_en)),
                        'content_en' => trim($r['content_en']),
                        'categories' => implode('|', $categories),
                    ];
                }
            }
        }

        return $output;
    }

    private function mapBlocksData($data, $type)
    {
        if ($type === '01. Artikel zonder afbeelding') {
            // Default article block
            return [
                'mainContent' => $data['tekst'],
            ];
        }

        if ($type === '02. Artikel met afbeelding') {
            // Video Block
            if ($data['vimeoid']) {
                return [
                    'mainContent' => $data['tekst'],
                    'videoId' => $data['vimeoid'],
                    'videoPosition' => '',
                ];
            }

            // Image block
            return [
                'mainContent' => $data['tekst'],
                'image' => '',
                'imagePosition' => '',
            ];
        }

        // extend code with other blocks

        return $data;
    }

    private function mapProfessionalData($data)
    {
        $categories = [];

        if ($data['ace'] === 'Ja') {
            $categories[] = 3294;
        }
        if ($data['csg'] === 'Ja') {
            $categories[] = 3295;
        }
        if ($data['cnl'] === 'Ja') {
            $categories[] = 3296;
        }
        if ($data['cew'] === 'Ja') {
            $categories[] = 3297;
        }

        $jobs = [];

        $beroepMapper = [
            ''                                           => 'none',
            'algemeen-arts'                              => 'algemeenArts',
            'algemeen-arts-somnoloog'                    => 'algemeenArts',
            'arts-verstandelijk-gehandicapten'           => 'artsVerstandelijkGehandicapten',
            'arts-verstandelijk-gehandicapten-somnoloog' => 'artsVerstandelijkGehandicapten',
            'bewegingsagoog'                             => 'bewegingsagoog',
            'coordinator-slaapverpleegkundigen'          => 'coordinatorSlaapverpleegkundigen',
            'dietist'                                    => 'dietist',
            'ergotherapeut'                              => 'ergotherapeut',
            'fysiotherapeut'                             => 'fysiotherapeut',
            'gedragswetenschapper'                       => 'gedragswetenschapper',
            'gedragswetenschapper-somnoloog'             => 'gedragswetenschapper',
            'gz-psycholoog'                              => 'gzPsycholoog',
            'hoofd-medische-dienst'                      => 'hoofdMedischeDienst',
            'huisarts'                                   => 'huisarts',
            'kinderarts'                                 => 'kinderarts',
            'kinderarts-somnoloog'                       => 'kinderarts',
            'kinderneuroloog'                            => 'kinderneuroloog',
            'kinderneuroloog-somnoloog'                  => 'kinderneuroloog',
            'kinder-en-jeugdpsychiater'                  => 'kinderEnJeugdpsychiater',
            'kinder-en-jeugdpsychiater-somnoloog'        => 'kinderEnJeugdpsychiater',
            'klinisch neuropsycholoog'                   => 'klinischNeuropsycholoog',
            'logopedist'                                 => 'logopedist',
            'longarts'                                   => 'longarts',
            'longarts-somnoloog'                         => 'longarts',
            'maatschappelijk-werker'                     => 'maatschappelijkWerker',
            'neuroloog'                                  => 'neuroloog',
            'neuroloog-somnoloog'                        => 'neuroloog',
            'onderzoeker'                                => 'onderzoeker',
            'orthopedagoog'                              => 'orthopedagoog',
            'physician-assistant'                        => 'physicianAssistant',
            'praktisch-pedagogisch-thuisbegeleider'      => 'praktischPedagogischThuisbegeleider',
            'promovendus'                                => 'promovendus',
            'regiebehandelaar'                           => 'regiebehandelaar',
            'somnoloog'                                  => 'somnoloog',
            'verpleegkundig-specialist'                  => 'verpleegkundigSpecialist'
        ];

        $jobs[] = $beroepMapper[$data['beroep']];

        if ($data['somnoloog'] === 'Ja') {
            $jobs[] = 'somnoloog';
        }

        $nlMapper = [
            'Nee'       => 'none',
            'nvt'       => 'none',
            'ACE'       => 'ace',
            'MUMC+'     => 'mumc',
            'ACE/MUMC+' => 'aceMumc'
        ];

        $odMapper = [
            ''    => 'none',
            'GWD' => 'gwd',
            'MW'  => 'mw',
            'PMD' => 'pmd',
            'PPT' => 'ppt'
        ];

        return [
            'titel'                => $data['titel'],
            'bigNummer'            => array_key_exists('big', $data) ? $data['big'] : '',
            'beknopteCv'           => $data['beknopte-cv'],
            'beknopteCvEn'         => array_key_exists('beknopte-cv-en', $data) ? $data['beknopte-cv-en'] : '',
            'aandachtsgebieden'    => $data['aandachtsgebieden'],
            'aandachtsgebiedenEn'  => array_key_exists('aandachtsgebieden-en', $data) ? $data['aandachtsgebieden-en'] : '',
            'voornaam'             => $data['voornaam'],
            'tussenvoegsel'        => $data['tussenvoegsel'],
            'twitter'              => $data['twitter'],
            'linkedin'             => $data['linkedin'],
            'isPromovendus'        => $data['promovendus'] === 'Ja',
            'isHoogleeraar'        => $data['hoogleraar'] === 'Ja',
            'ondersteunendeDienst' => array_key_exists('ondersteunende-dienst', $data) ? $odMapper[$data['ondersteunende-dienst']] : 'none',
            'locatieNeuroloog'     => array_key_exists('locatie-neuroloog', $data) ? $nlMapper[$data['locatie-neuroloog']] : 'none',
            'beroep'               => implode('|', $jobs),
            'profsCategory'        => implode('|', $categories),
        ];
    }


    private function mapFields($fields)
    {
        $newArray = [];

        foreach ($fields as $field) {
            $name = $this->slugify->slugify($field['name']);
            $newArray[$name] = $field['value'];
        }

        // Remove literatuur lijst items
        if (array_key_exists('literatuurlijst', $newArray)) {
            unset($newArray['literatuurlijst']);
        }

        // Case convert beroepen to avoid errors
        if (array_key_exists('beroep', $newArray)) {
            $newArray['beroep'] = $this->slugify->slugify($newArray['beroep']);
        }

        return $newArray;
    }


    public function generateGlossaryJSONExport()
    {
        $items = $this->getAllGlossaryItems();

        $fp = fopen( __DIR__ . '/../dumps/zoekwoorden.nl.json', 'w');
        fwrite($fp, json_encode($items['nl']));
        fclose($fp);

        $fp = fopen(__DIR__ . '/../dumps/zoekwoorden.en.json', 'w');
        fwrite($fp, json_encode($items['en']));
        fclose($fp);

        echo 'Dump succesfull!';
    }

    public function generateProffessionalsJSONExport()
    {
        $items = $this->getProfessionalsByParentId(86);

        $fp = fopen( __DIR__ . '/../dumps/profs.json', 'w');
        fwrite($fp, json_encode($items));
        fclose($fp);
    }


    public function generateNewsJSONExport()
    {
        $items = $this->getPagesByParentId(83);

        $fp = fopen( __DIR__ . '/../dumps/news.json', 'w');
        fwrite($fp, json_encode($items));
        fclose($fp);
    }
}
